import React, { useState, useEffect } from "react";
import { Carousel } from "antd";
import axios from "../../../API/axios";
import requests from "../../../API/requests";
import { Row, Col, Button, Container } from "react-bootstrap";

const Header = () => {
  const contentStyle = {
    height: "271px",
    color: "#fff",
    lineHeight: "271px",
    textAlign: "center",
    background: "#364d79",
  };
  const [movies, setMovies] = useState([]);
  const base_url = "https://image.tmdb.org/t/p/original/";
  useEffect(() => {
    const fetchTrendingMovies = async () => {
      const moviesTrending = await axios.get(requests.fetchTrending);
      setMovies(moviesTrending.data.results);
      return moviesTrending;
    };
    fetchTrendingMovies();
  }, []);
  let countUpper = 0;
  let countLower = 0;
  let randomView = Math.floor(1000000 + Math.random() * 9000000);
  console.log(randomView);
  return (
    <Container fluid className="header-container">
      <Row className="upper_picture">
        <Col xs={9} className="px-1">
          <Carousel className="left_large_picture w-100" autoplay>
            {movies.map((movie) => (
              <div className="wrap_img" style={contentStyle}>
                <div className="wrap_img__content">
                  <img
                    className="treding_img"
                    src={`${base_url}${movie?.backdrop_path}`}
                    alt="movieTrending"
                  />
                  <div className="anime_des">
                    <div className="views">
                      {randomView.toLocaleString()} views
                    </div>
                    <div className="name">
                      {movie?.original_name || movie?.original_title}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </Carousel>
        </Col>
        <Col xs={3} className="px-1">
          <div className="right_picture">
            {movies.map((movie) => {
              if (countUpper > 2) return "";
              countUpper++;
              return (
                <div className="normal_picture mb-2">
                  <img
                    className="normal_picture__img"
                    src={`${base_url}${movie?.backdrop_path}`}
                    alt="img"
                  />
                </div>
              );
            })}
          </div>
        </Col>
      </Row>
      <Row className="lower_picture pt-5">
        {movies.map((movie) => {
          if (countLower > 3) return "";
          countLower++;
          return (
            <Col xs={3} className="normal_picture px-1">
              <img
                className="normal_picture__img"
                src={`${base_url}${movie?.backdrop_path}`}
                alt="img"
              />
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};

export default Header;

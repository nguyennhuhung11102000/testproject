import React from "react";
import { LIST_IMG } from "../../../Assets/img/index";
import { Input } from "antd";
import { SearchOutlined, UserOutlined, BulbOutlined } from "@ant-design/icons";
import { Row, Col, Container } from "react-bootstrap";

const NavBar = () => {
  return (
    <Container
      fluid
      className="nav-container position-fixed top-0 left-0 right-0"
    >
      <Row className="">
        <Col className="left">
          <Row className="d-flex justify-content-between w-100">
            <Col xs={3}>
              <img className="logo_img h-100" src={LIST_IMG.logo} alt="logo" />
            </Col>
            <Col xs={9}>
              <ul className="category_list w-100 d-flex align-items-center ps-5 h-100">
                <li className="category_list__item">Anime</li>
                <li className="category_list__item">Video</li>
                <li className="category_list__item">Tin Tức</li>
                <li className="category_list__item">Truyện</li>
                <li className="category_list__item">BXH</li>
              </ul>
            </Col>
          </Row>
        </Col>
        <Col className="right">
          <Row className="h-100 d-flex align-items-center w-100">
            <Col>
              <div className="search h-100 d-flex align-items-center">
                <Input placeholder="Tìm kiếm anime" />
                <SearchOutlined />
              </div>
            </Col>
            <Col className="h-100 d-flex justify-content-end">
              <div className="darkmode">
                <BulbOutlined />
              </div>
              <div className="user">
                <UserOutlined />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default NavBar;

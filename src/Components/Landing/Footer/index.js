import React from 'react'

const Footer = () => {
    return (
        <div className="wrap_footer">
            <div className="wrap_footer__container">
                <div className="wrap_footer__container__content">this is footer</div>
            </div>
        </div>
    )
}

export default Footer

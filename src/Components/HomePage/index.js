import React from "react";
import Category from "../Category";
import requests from "../../API/requests";
import { Container } from "react-bootstrap";

const HomePage = () => {
  return (
    <Container fluid>
      <Category title="TẬP MỚI NHẤT >" fetchUrl={requests.fetchTrending} />
      <Category
        title="PHIM HOẠT HÌNH >"
        fetchUrl={requests.fetchActionMovies}
      />
      <Category
        title="TRUYỆN TRANH >"
        fetchUrl={requests.fetchComedyMovies}
        isTallRow
      />
      <Category
        title="BỘ SƯU TẬP >"
        fetchUrl={requests.fetchHorrorMovies}
        isCollection
      />
      <Category
        title="HÔM NAY XEM GÌ >"
        fetchUrl={requests.fetchRomanceMovies}
      />
      <Category
        title="BẢNG XẾP HẠNG >"
        fetchUrl={requests.fetchDocumentaries}
      />
    </Container>
  );
};

export default HomePage;

import React, { useState, useEffect } from "react";
import axios from "../../API/axios";
import { Row, Col, Button, Container } from "react-bootstrap";

const Category = ({ title, fetchUrl, halfSize, isTallRow, isCollection }) => {
  const base_url = "https://image.tmdb.org/t/p/original/";
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    const getListMovie = async () => {
      const request = await axios.get(fetchUrl);
      setMovies(request.data.results);
      return request;
    };
    getListMovie();
  }, [fetchUrl]);
  let randomView = Math.floor(10000 + Math.random() * 90000);
  const truncate = (string, n) => {
    return string?.length > 65 ? `${string.substring(0, n)} ...` : string;
  };
  const movieTall = [];
  for (let i = 0; i <= 5; i++) {
    movieTall.push(movies[i]);
  }
  console.log(movies);
  const moviesSeason = [];
  for (let i = 0; i <= 3; i++) {
    moviesSeason.push(movies[i]);
  }
  const newestEpisode = [];
  for (let i = 0; i <= 11; i++) {
    newestEpisode.push(movies[i]);
  }
  const smallMovies = [];
  for (let i = 0; i <= 11; i++) {
    smallMovies.push(movies[i]);
  }
  return (
    <div className="wrap-category">
      <Row className="my-3">
        <h2>{title}</h2>
      </Row>
      {halfSize ? (
        <Row className="wrap_episodes_half">
          {smallMovies.map((movie) => (
            <Col xs={3} className="episode_halfheight px-1">
              <div className="background_halfheight">
                <img
                  src={`${base_url}${
                    movie?.backdrop_path || movie?.poster_path
                  }`}
                  alt="img"
                />
              </div>
              <div className="wrap_info_halfheight">
                <div className="wrap_des_halfheight">
                  {truncate(movie?.overview, 65)}
                </div>
              </div>
            </Col>
          ))}
        </Row>
      ) : isTallRow ? (
        <Row className="wrap_episodes_tall">
          {movieTall?.map((movie) => (
            <Col xs={2} className="episode_tall px-1">
              <div className="background_episode_tall ">
                <img
                  src={`${base_url}${
                    movie?.backdrop_path || movie?.poster_path
                  }`}
                  alt="img"
                />
              </div>
              <span className="chapter">Chapter 200</span>
              <div className="name">
                {movie?.original_title || movie?.original_name}
              </div>
              <div className="fade_bottom" />
            </Col>
          ))}
        </Row>
      ) : isCollection ? (
        <Row className="wrap_episodes_season">
          {moviesSeason.map((movie) => (
            <Col xs={3} className="episode_season px-1">
              <div className="background_episode_season ">
                <img
                  src={`${base_url}${
                    movie?.backdrop_path || movie?.poster_path
                  }`}
                  alt="img"
                />
              </div>
            </Col>
          ))}
        </Row>
      ) : (
        <Row className="wrap_episodes">
          {newestEpisode.map((movie) => (
            <Col xs={3} className="episode mb-2 px-1">
              <div className="background_episode ">
                <img
                  class="img"
                  src={`${base_url}${
                    movie?.backdrop_path || movie?.poster_path
                  }`}
                  alt="img"
                />
                <div className="fade_bottom" />
              </div>
              <div className="wrap_info">
                <div className="name">
                  {movie?.original_title || movie?.original_name}
                </div>
                <div className="wrap_des">
                  <div className="current_episode">
                    {truncate(movie?.overview, 15)}
                  </div>
                  <div className="views">{randomView} lượt xem</div>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      )}
    </div>
  );
};

export default Category;
